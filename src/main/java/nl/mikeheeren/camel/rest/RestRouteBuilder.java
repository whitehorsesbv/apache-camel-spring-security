package nl.mikeheeren.camel.rest;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class RestRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {
        rest()
            .get("/public")
                .description("The public API is available for everyone.")
                .route().routeId("public")
                    .setBody(constant("Hello world!"))
                .endRest()
            .get("/secured")
                .description("The secured API is only accessible for authenticated users.")
                .route().routeId("secured")
                    .setBody(constant("Hello user!"))
                .endRest()
            .get("/secured/admin")
                .description("The admin part of the API requires the user to be authorized with the ADMIN role.")
                .route().routeId("admin")
                    .policy("adminPolicy")
                    .setBody(constant("Hello admin!"))
                .endRest();
    }

}
